import settings
from decimal import Decimal
from collections import OrderedDict


class Product(object):
    """
    Data container for product object
    """
    def __init__(self, idx: int, name: str, price_normal: Decimal,
                 amount: int = None, discount_price: Decimal = None):
        """
        :param idx: Product ID
        :param name: Name of product
        :param price_normal: Regular price
        :param amount: Amount for discount (optional)
        :param discount_price: Discount price (optional)
        """
        self.id = idx
        self.name = name
        self.price_normal = Decimal(price_normal).quantize(Decimal('0.01'))
        self.amount = amount
        self.discount_price = Decimal(discount_price).quantize(Decimal('0.01'))

    def __str__(self):
        return self.name


class CartItem(object):
    def __init__(self, product, count):
        self.product = product
        self.count = count

    def __str__(self):
        discount = ' [DISCOUNT]' if self.check_discount() else ''
        return f'{self.product.name} ({self.count} pcs) = {self.get_total_price()}{settings.CURRENCY_SYMBOL}{discount}'

    def check_discount(self):
        return self.count >= self.product.amount

    def get_total_price(self):
        current_price = self.product.price_normal
        if self.check_discount():
            current_price = self.product.discount_price

        return current_price * self.count


class Cart(object):
    def __init__(self):
        self.items = OrderedDict()

    def add_item(self, item):
        if item.product.id not in self.items:
            # If product doesn't exist in cart, add it
            self.items[item.product.id] = item
        else:
            # If product already exists, update count
            self.items[item.product.id].count += item.count

    def get_items(self):
        return self.items.values()
