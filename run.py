import settings
import logging
from utils import load_csv
from service import ShopService

logger = logging.getLogger(__name__)


def main():
    products = load_csv(settings.CSV_FILE_NAME)

    service = ShopService()
    service.load_products(products)

    while True:
        service.display_available_products()
        try:
            product = products[int(input('Input product ID: ')) - 1]
        except ValueError:
            logger.error('Invalid product ID')
            continue
        except IndexError:
            logger.error('No such product')
            continue

        try:
            count = int(input('Input count: '))
        except ValueError:
            logger.error('Invalid count')
            continue

        service.buy_product(product, count)

        if input('Continue? (press [Y] or n):').lower() == 'n':
            break

    service.display_cart()


if __name__ == '__main__':
    main()
