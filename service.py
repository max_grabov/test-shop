import logging
from models import Cart, CartItem
from exceptions import ProductsNotLoadedError


class ShopService:
    def __init__(self):
        self.cart = Cart()
        self.products = None
        self.logger = logging.getLogger(__name__)

        self.logger.info('Shop service initialized')

    def load_products(self, products):
        self.products = products

    def get_products(self):
        return self.products

    def display_available_products(self):
        self._print_message('Available products:')
        self._print_message('\t'.join(['#', 'Name', 'price', 'amount', 'discount_price']))
        for product in self.products:
            self._print_message(
                '\t'.join([
                    str(product.id),
                    product.name,
                    str(product.price_normal),
                    str(product.amount),
                    str(product.discount_price)
                ])
            )

    def buy_product(self, product, count):
        if self.products is None:
            raise ProductsNotLoadedError('Products have not been loaded.')
        item = CartItem(product=product, count=count)
        self.cart.add_item(item)
        self._print_message(f'Product {product} has been added to your cart.')

    def display_cart(self):
        self._print_message('Products in your cart:')
        for item in self.cart.get_items():
            self._print_message(item)

    @staticmethod
    def _print_message(message):
        print(message)
