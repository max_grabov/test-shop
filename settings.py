import os
import logging

DEBUG = True

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
CSV_FILE_NAME = os.path.join(BASE_DIR, 'products.csv')
CURRENCY_SYMBOL = '\u20BD'


logging.basicConfig(level=logging.ERROR)
