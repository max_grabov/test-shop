from decimal import Decimal
from unittest import TestCase
from models import Product


class ProductTestCase(TestCase):

    def setUp(self):
        self.product = Product(1, 'name', Decimal('1.35'), 5, Decimal('1.1'))

    def test_product_init(self):
        self.assertEqual(self.product.id, 1)
        self.assertEqual(self.product.name, 'name')
        self.assertEqual(self.product.price_normal, Decimal('1.35'))
        self.assertEqual(self.product.amount, 5)
        self.assertEqual(self.product.discount_price, Decimal('1.1'))

    def test_str(self):
        self.assertEqual(str(self.product), 'name')
