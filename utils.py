import csv
import logging
import settings
from decimal import Decimal

from models import Product


logger = logging.getLogger(__name__)


def load_csv(file_path):
    items = []
    with open(file_path) as csv_file:
        reader = csv.DictReader(csv_file)
        for idx, row in enumerate(reader, start=1):
            try:
                product = Product(
                    idx=idx,
                    name=row['Name'],
                    price_normal=Decimal(row['price_normal']).quantize(Decimal('0.01')),
                    amount=int(row['amount']),
                    discount_price=Decimal(row['discount_price']).quantize(Decimal('0.01'))
                )
                items.append(product)
            except Exception:
                message = f'Row number {idx} is invalid'
                if settings.DEBUG:
                    logger.exception(message)
                else:
                    logger.error(message)

    return items
